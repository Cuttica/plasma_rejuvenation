import java.io.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * This file replicates the fitness.sh replacing file removal polling with system monitor
 * */
public class Fitness {
	public static void main(String[] args) throws IOException, InterruptedException {
//		[[ ! "$UGP3_FITNESS_FILE" ]] && UGP3_FITNESS_FILE=fitness.out
		
		File ugpFintnessFile = new File("fitness.out");
		
		
		File fromUGP = new File("individualsToEvaluate.txt");
		
		File toUGP = new File("evaluator/to.ugp");
		
		// trigger evaluation - evaluator waits until file is deleted
		while (true) {
			toUGP.delete();
			if (toUGP.exists())
				try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("failed to delete.txt", true)))) {
					out.println(System.currentTimeMillis() + ": " + toUGP);
					Thread.sleep(100);
				}catch (Exception e) {
				}
			else
				break;
		}
		
		
		
		System.out.println(toUGP + " was removed to trigger evaluation. Waiting when our product, " + fromUGP + ", is deleted");
		WatchService watcher = FileSystems.getDefault().newWatchService();
		
		Path dir = Paths.get(".");
		dir.register(watcher, ENTRY_DELETE);
		
		System.out.println("waiting for " + fromUGP + " to disappear");
		while (fromUGP.exists()) {
			
		    WatchKey key = watcher.take();

		    for (WatchEvent<?> event: key.pollEvents()) {}
		    if (!key.reset()) throw new IllegalStateException("File monitor key failed");
		}		
		
		
//		#We need to duplicate $toUGP with $UGP3_FITNESS_FILE because it must be deleted only
//		# when uGP has produced next intidivudalToEvaluate while
//		# uGP removes the fitness result prematurely, during the individual production.
		
		Files.copy(Paths.get(toUGP.toURI()), Paths.get(ugpFintnessFile.toURI()), java.nio.file.StandardCopyOption.REPLACE_EXISTING);

		// print the score produced
		Files.copy(Paths.get(toUGP.toURI()), System.out);
		

	}
}