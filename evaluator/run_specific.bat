
: Evaluator simulates a prescribed set of tests at start and produces to.ugp file
: with their Hamming. You may ignore it. When that file disappears, it is expected
: that uGP has prepared the next test for evaluation in 'from.gp'. This file is simulated
: and Hamming is produced. Waiting for 'to.gp' removal and writing to 'from.gp'
: continues until there is not 'from.gp' after 'to.gp' removal.

setlocal
set path=%~dp0/asm;c:\cygwin64\bin;%path%

:classpath to current dir is needed for classes in distutils
set CLASSPATH=%~dp0

::generate random number, suitable for parallel execution, http://stackoverflow.com/a/19705062/1083704
@for /f %%i in ('C:\cygwin64\bin\bash -c "echo $RANDOM"') do @set CYGWIN_RANDOM=%%i
:set ZAMIA_DATA_DIR=zamia_temp-%CYGWIN_RANDOM%

: my working zamia
set args=-p "PlasmaEvaluator-%CYGWIN_RANDOM%" -q -f plasma_rej_userworkload_specific_overhead.py -s "random_tests=False" "worst_fitness=False"
:set "ZAMIA_DATA_DIR=work" && del ..\ugp3.lok && zamiacad -q -f userload-specific_overhead.py -s "design='ALU4'"

call zamiacad.bat %args%

endlocal