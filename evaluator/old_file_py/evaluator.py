from __future__ import with_statement
import os, subprocess

simtime = "4000"

tbFiles = [#"bug.vhd", 
	"tbench.vhd"]
	
FNULL = open(os.devnull,"w")

#http://superuser.com/questions/674506 explains why we need to retry
#the file delete operation
def ensureRemove(file):
	while (True):
		try:
			os.remove(file)
			break
		except:
			from time import ctime
			with open ("failed to remove.txt", 'a') as f: f.write ("%s: failed to remove %s\n" % (ctime(), file))
			time.sleep (.2)

def compile(asmPath):

	#os.system("asm.bat < " + asmPath + " 2> nul")
	
	with open(asmPath, 'r') as f:
		
		#raw_input("asm.bat < " + asmPath + " > null")
		subprocess.check_call("asm.bat", stderr=FNULL, stdin=f)

	for file in os.listdir("."):
		if file == "asm.o" or file.startswith("test."):
			ensureRemove(file)
			
testExtension = ".asm"

execfile("../../b01/evaluator/strategy2.py")
#import subprocess
#subprocess.call(["D:\\Documenti\\GitHub\\ugp-rejuvenation\\evaluator\\gen_universal.bat", "ALU32", "-best", "1"])