
@echo off
setlocal

set path=%~dp0/asm;c:\cygwin64\bin;%path%

:classpath to current dir is needed for classes in distutils
set CLASSPATH=%~dp0

::generate random number, suitable for parallel execution, http://stackoverflow.com/a/19705062/1083704
@for /f %%i in ('C:\cygwin64\bin\bash -c "echo $RANDOM"') do @set CYGWIN_RANDOM=%%i
:set ZAMIA_DATA_DIR=zamia_temp-%CYGWIN_RANDOM%

: my working zamia
:set args=-p "PlasmaEvaluator-%CYGWIN_RANDOM%" -q -f testCompiler.py

:call C:\Users\valentin\workspace\zamiacad\zamiacad.bat %args%
call python testCompiler.py

: They can also use the GUI

:if you want to use Plasma + dump for ALU-Rejuvenation uncomment these following lines

:cd D:\Documenti\GitHub\ugp-rejuvenation\evaluator
:d:
:call D:\Documenti\GitHub\ugp-rejuvenation\evaluator\gen_universal.bat ALU32 -best 1

endlocal

pause