from __future__ import with_statement
import os, subprocess
from shutil import copyfile

simtime = "4000"

tbFiles = [#"bug.vhd",
	"tbench.vhd"]

FNULL = open(os.devnull,"w")

#http://superuser.com/questions/674506 explains why we need to retry
#the file delete operation
def ensureRemove(file):
	while (True):
		try:
			os.remove(file)
			break
		except:
			from time import ctime
			with open ("failed to remove.txt", 'a') as f: f.write ("%s: failed to remove %s\n" % (ctime(), file))
			time.sleep (.2)

def compile(asmPath):

	#os.system("asm.bat < " + asmPath + " 2> nul")

	with open(asmPath, 'r') as f:

		#raw_input("asm.bat < " + asmPath + " > null")
		subprocess.check_call("asm.bat", stderr=FNULL, stdin=f)

	for file in os.listdir("."):
		if file == "asm.o" or file.startswith("test."):
			ensureRemove(file)

testExtension = ".asm"


import os, time

CurDir = "./"
testDir = CurDir + "asm_test/"
compiledFile = 'code.txt'
compiledDir = 'compiled_files/'
if compiledFile not in os.listdir(CurDir):
	with open(compiledFile,'w') as f: f.write('')

for test_tile in os.listdir(testDir):
	if test_tile.endswith('.asm'):
		ensureRemove(compiledFile)
		print "######################################################################################################"
		print 'start compiling ' + test_tile
		compile(testDir + test_tile)
		print 'end compiling!'
		print "######################################################################################################\n\n"
		if compiledFile not in os.listdir(CurDir):
			print 'Compilation Error in file ' + test_tile
			break
		else:
			copyfile(compiledFile, compiledDir + test_tile[:-3] + "txt")
		#time.sleep (.5)
