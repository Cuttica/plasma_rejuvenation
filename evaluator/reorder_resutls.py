
position = "./wl_tests/"#"./conclusions/REJUVENATIONS_WITH_RANDOMS/1/"

with open(position + "best_0.tst-rejuvenated.txt", "r") as f:
	values = []
	test_names = []
	for i, line in enumerate(f.readlines()):
		line = line.strip()
		if i > 3 and line != "":
			if i == 4:
				test_names = line.split(",")
			else:
				values.append(line.split(","))

	order = ["delay", "and.asm", "or.asm", "nor.asm", "xor.asm", "rand_2.asm", "rand_3.asm", "rand_1.asm", "add.asm", "sub.asm", "div.asm", "mul.asm", "shift.asm", "mixed.asm", "nop.asm", "worst_ugp.asm"]

	strr = ""
	for val in values:
		for v in val:
			strr += " %15s " % str(v)
		strr += "\n"
	print strr

	new_values = []

	for vett in values:
		new_vet = vett[:]
		new_values.append(new_vet)

	for i in range(len(order)):
		for j in range(len(test_names)):
			if order[i] == test_names[j]:
				for k in range(len(values)-1):
					print k
					new_values[k][i] = values[k][j]

	strr = "".join(" %13s " % str(ele) for ele in order) + "\n"
	for val in new_values:
		for v in val:
			strr+= " %13s " % str(v)
		strr += "\n"
	print strr

	with open(position + "best_0.tst-rejuvenated_mod.csv","w") as f:
		text = "".join(str(ele ) + ", " for ele in order) + "\n"
		for overhead in new_values:
			for value in overhead:
				text += str(value) + ", "
			text += "\n"
		f.write(text)