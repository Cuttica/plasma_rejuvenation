	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	ori $2,$0,10
	ori $3,$0,5
	multu $2,$3
	nop
	ori $2,$0,124
	ori $3,$0,28
	multu $2,$3
	nop
	ori $2,$0,82
	ori $3,$0,9
	multu $2,$3
	nop
	ori $2,$0,19
	ori $3,$0,45
	multu $2,$3
	nop
	ori $2,$0,31
	ori $3,$0,83
	multu $2,$3
	nop
	ori $2,$0,88
	ori $3,$0,30
	multu $2,$3
	nop
	ori $2,$0,63
	ori $3,$0,69
	multu $2,$3
	nop
	ori $2,$0,55
	ori $3,$0,49
	multu $2,$3
	nop
	ori $2,$0,114
	ori $3,$0,58
	multu $2,$3
	nop
	ori $2,$0,95
	ori $3,$0,37
	multu $2,$3
	nop
	ori $2,$0,21
	ori $3,$0,23
	multu $2,$3
	nop
	ori $2,$0,98
	ori $3,$0,73
	multu $2,$3
	nop
	ori $2,$0,112
	ori $3,$0,66
	multu $2,$3
	nop
	ori $2,$0,104
	ori $3,$0,68
	multu $2,$3
	nop
	ori $2,$0,38
	ori $3,$0,126
	multu $2,$3
	nop
	ori $2,$0,121
	ori $3,$0,86
	multu $2,$3
	nop
	ori $2,$0,61
	ori $3,$0,44
	multu $2,$3
	nop
	ori $2,$0,89
	ori $3,$0,96
	multu $2,$3
	nop
	ori $2,$0,43
	ori $3,$0,101
	multu $2,$3
	nop
	ori $2,$0,69
	ori $3,$0,2
	multu $2,$3
	nop


	.set reorder
	.end entry
