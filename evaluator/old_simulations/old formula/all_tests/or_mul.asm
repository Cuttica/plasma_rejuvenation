	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	li $2,12
	li $3,24
	multu $2,$3
	nop
	li $2,16
	li $3,61
	multu $2,$3
	nop
	li $2,18
	li $3,48
	multu $2,$3
	nop
	li $2,94
	li $3,17
	multu $2,$3
	nop
	li $2,7
	li $3,72
	multu $2,$3
	nop
	li $2,114
	li $3,87
	multu $2,$3
	nop
	li $2,77
	li $3,93
	multu $2,$3
	nop
	li $2,35
	li $3,17
	multu $2,$3
	nop
	li $2,78
	li $3,94
	multu $2,$3
	nop
	li $2,44
	li $3,35
	multu $2,$3
	nop
	li $2,20
	li $3,103
	multu $2,$3
	nop
	li $2,16
	li $3,98
	multu $2,$3
	nop
	li $2,44
	li $3,66
	multu $2,$3
	nop
	li $2,39
	li $3,109
	multu $2,$3
	nop
	li $2,28
	li $3,95
	multu $2,$3
	nop
	li $2,27
	li $3,66
	multu $2,$3
	nop
	li $2,3
	li $3,30
	multu $2,$3
	nop
	li $2,21
	li $3,58
	multu $2,$3
	nop
	li $2,127
	li $3,44
	multu $2,$3
	nop
	li $2,40
	li $3,116
	multu $2,$3
	nop
	li $2,74
	li $3,14
	or $4,$2,$3
	
	li $2,29
	li $3,114
	or $4,$2,$3
	
	li $2,22
	li $3,71
	or $4,$2,$3
	
	li $2,71
	li $3,52
	or $4,$2,$3
	
	li $2,68
	li $3,123
	or $4,$2,$3
	
	li $2,58
	li $3,23
	or $4,$2,$3
	
	li $2,123
	li $3,93
	or $4,$2,$3
	
	li $2,41
	li $3,4
	or $4,$2,$3
	
	li $2,123
	li $3,20
	or $4,$2,$3
	
	li $2,97
	li $3,24
	or $4,$2,$3
	
	li $2,47
	li $3,18
	or $4,$2,$3
	
	li $2,71
	li $3,113
	or $4,$2,$3
	
	li $2,111
	li $3,71
	or $4,$2,$3
	
	li $2,74
	li $3,101
	or $4,$2,$3
	
	li $2,3
	li $3,10
	or $4,$2,$3
	
	li $2,86
	li $3,95
	or $4,$2,$3
	
	li $2,95
	li $3,23
	or $4,$2,$3
	
	li $2,2
	li $3,0
	or $4,$2,$3
	
	li $2,95
	li $3,98
	or $4,$2,$3
	
	li $2,121
	li $3,23
	or $4,$2,$3
	


	.set reorder
	.end entry
