	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	li $2,56
	li $3,109
	or $4,$2,$3
	
	li $2,14
	li $3,13
	or $4,$2,$3
	
	li $2,79
	li $3,77
	or $4,$2,$3
	
	li $2,112
	li $3,72
	or $4,$2,$3
	
	li $2,27
	li $3,100
	or $4,$2,$3
	
	li $2,13
	li $3,69
	or $4,$2,$3
	
	li $2,38
	li $3,47
	or $4,$2,$3
	
	li $2,70
	li $3,74
	or $4,$2,$3
	
	li $2,33
	li $3,74
	or $4,$2,$3
	
	li $2,59
	li $3,54
	or $4,$2,$3
	
	li $2,8
	li $3,35
	or $4,$2,$3
	
	li $2,91
	li $3,124
	or $4,$2,$3
	
	li $2,83
	li $3,98
	or $4,$2,$3
	
	li $2,50
	li $3,101
	or $4,$2,$3
	
	li $2,8
	li $3,100
	or $4,$2,$3
	
	li $2,122
	li $3,26
	or $4,$2,$3
	
	li $2,64
	li $3,52
	or $4,$2,$3
	
	li $2,37
	li $3,80
	or $4,$2,$3
	
	li $2,81
	li $3,57
	or $4,$2,$3
	
	li $2,100
	li $3,41
	or $4,$2,$3
	
	li $3,123
	li $4,35
	sub $2,$3,$4
	
	li $3,104
	li $4,77
	sub $2,$3,$4
	
	li $3,51
	li $4,124
	sub $2,$3,$4
	
	li $3,35
	li $4,12
	sub $2,$3,$4
	
	li $3,21
	li $4,59
	sub $2,$3,$4
	
	li $3,123
	li $4,74
	sub $2,$3,$4
	
	li $3,103
	li $4,86
	sub $2,$3,$4
	
	li $3,42
	li $4,87
	sub $2,$3,$4
	
	li $3,23
	li $4,47
	sub $2,$3,$4
	
	li $3,65
	li $4,56
	sub $2,$3,$4
	
	li $3,120
	li $4,82
	sub $2,$3,$4
	
	li $3,53
	li $4,20
	sub $2,$3,$4
	
	li $3,48
	li $4,124
	sub $2,$3,$4
	
	li $3,38
	li $4,83
	sub $2,$3,$4
	
	li $3,0
	li $4,34
	sub $2,$3,$4
	
	li $3,23
	li $4,34
	sub $2,$3,$4
	
	li $3,44
	li $4,98
	sub $2,$3,$4
	
	li $3,19
	li $4,94
	sub $2,$3,$4
	
	li $3,28
	li $4,38
	sub $2,$3,$4
	
	li $3,1
	li $4,115
	sub $2,$3,$4
	


	.set reorder
	.end entry
