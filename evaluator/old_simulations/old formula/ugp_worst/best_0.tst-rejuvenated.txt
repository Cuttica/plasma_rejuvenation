universal rejuvenation
delay when all inputs zero prob = 1.0 is 983.46
delay when all inputs zero prob = 0.0 is 635.33

delay,worst_ugp.asm
0%,812.96
1e-10%,812.68
1e-09%,810.91
1e-08%,805.53
1e-07%,797.74
1e-06%,788.76
1e-05%,778.81
0.0001%,767.86
0.001%,755.90
0.01%,742.94
0.05%,733.30
0.1%,729.01
0.2%,724.64
0.5%,718.76
1%,714.23
10%,698.76
100%,681.65
there are 556 nets with pz in range (0.9999999999978891, 1)
