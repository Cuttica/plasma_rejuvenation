	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	li $3,116
	li $4,94
	sub $2,$3,$4
	
	li $3,104
	li $4,104
	sub $2,$3,$4
	
	li $3,34
	li $4,69
	sub $2,$3,$4
	
	li $3,118
	li $4,43
	sub $2,$3,$4
	
	li $2,4
	li $3,106
	nor $4,$2,$3
	
	li $2,83
	li $3,100
	nor $4,$2,$3
	
	li $2,78
	li $3,109
	nor $4,$2,$3
	
	li $2,79
	li $3,5
	nor $4,$2,$3
	
	li $3,102
	li $4,81
	addu $2,$3,$4
	
	li $3,99
	li $4,34
	addu $2,$3,$4
	
	li $3,114
	li $4,63
	addu $2,$3,$4
	
	li $3,108
	li $4,76
	addu $2,$3,$4
	
	li $2,89
	li $3,86
	xor $4,$2,$3
	
	li $2,102
	li $3,82
	xor $4,$2,$3
	
	li $2,94
	li $3,49
	xor $4,$2,$3
	
	li $2,9
	li $3,86
	xor $4,$2,$3
	
	li $2,123
	li $3,53
	multu $2,$3
	nop
	li $2,16
	li $3,102
	multu $2,$3
	nop
	li $2,126
	li $3,96
	multu $2,$3
	nop
	li $2,61
	li $3,119
	multu $2,$3
	nop
	li $2,47
	li $3,14
	xor $4,$2,$3
	
	li $2,114
	li $3,73
	xor $4,$2,$3
	
	li $2,33
	li $3,100
	xor $4,$2,$3
	
	li $2,64
	li $3,126
	xor $4,$2,$3
	
	li $2,124
	li $3,88
	divu $2,$3
	nop
	li $2,114
	li $3,72
	divu $2,$3
	nop
	li $2,123
	li $3,114
	divu $2,$3
	nop
	li $2,121
	li $3,43
	divu $2,$3
	nop
	li $2,108
	li $3,68
	nor $4,$2,$3
	
	li $2,120
	li $3,45
	nor $4,$2,$3
	
	li $2,74
	li $3,50
	nor $4,$2,$3
	
	li $2,75
	li $3,120
	nor $4,$2,$3
	
	li $2,65
	li $3,106
	multu $2,$3
	nop
	li $2,10
	li $3,80
	multu $2,$3
	nop
	li $2,42
	li $3,120
	multu $2,$3
	nop
	li $2,25
	li $3,11
	multu $2,$3
	nop
	li $2,127
	li $3,126
	multu $2,$3
	nop
	li $2,109
	li $3,88
	multu $2,$3
	nop
	li $2,106
	li $3,34
	multu $2,$3
	nop
	li $2,55
	li $3,122
	multu $2,$3
	nop
	li $2,52
	li $3,13
	xor $4,$2,$3
	
	li $2,126
	li $3,3
	xor $4,$2,$3
	
	li $2,114
	li $3,3
	xor $4,$2,$3
	
	li $2,22
	li $3,115
	xor $4,$2,$3
	
	li $2,101
	li $3,74
	and $4,$2,$3
	
	li $2,89
	li $3,21
	and $4,$2,$3
	
	li $2,37
	li $3,107
	and $4,$2,$3
	
	li $2,78
	li $3,49
	and $4,$2,$3
	
	li $3,93
	li $4,43
	addu $2,$3,$4
	
	li $3,58
	li $4,72
	addu $2,$3,$4
	
	li $3,87
	li $4,33
	addu $2,$3,$4
	
	li $3,46
	li $4,25
	addu $2,$3,$4
	
	li $2,28
	li $3,33
	xor $4,$2,$3
	
	li $2,49
	li $3,49
	xor $4,$2,$3
	
	li $2,99
	li $3,7
	xor $4,$2,$3
	
	li $2,57
	li $3,94
	xor $4,$2,$3
	
	li $3,75
	li $4,96
	sub $2,$3,$4
	
	li $3,31
	li $4,125
	sub $2,$3,$4
	
	li $3,2
	li $4,25
	sub $2,$3,$4
	
	li $3,58
	li $4,21
	sub $2,$3,$4
	
	li $2,64
	li $3,73
	multu $2,$3
	nop
	li $2,50
	li $3,14
	multu $2,$3
	nop
	li $2,38
	li $3,47
	multu $2,$3
	nop
	li $2,22
	li $3,109
	multu $2,$3
	nop
	li $2,55
	li $3,37
	xor $4,$2,$3
	
	li $2,96
	li $3,77
	xor $4,$2,$3
	
	li $2,55
	li $3,113
	xor $4,$2,$3
	
	li $2,43
	li $3,1
	xor $4,$2,$3
	
	li $3,25
	li $4,54
	sub $2,$3,$4
	
	li $3,6
	li $4,91
	sub $2,$3,$4
	
	li $3,96
	li $4,114
	sub $2,$3,$4
	
	li $3,53
	li $4,108
	sub $2,$3,$4
	
	li $2,81
	li $3,91
	or $4,$2,$3
	
	li $2,72
	li $3,111
	or $4,$2,$3
	
	li $2,12
	li $3,0
	or $4,$2,$3
	
	li $2,82
	li $3,73
	or $4,$2,$3
	
	li $3,48
	li $4,122
	sub $2,$3,$4
	
	li $3,123
	li $4,67
	sub $2,$3,$4
	
	li $3,0
	li $4,42
	sub $2,$3,$4
	
	li $3,14
	li $4,116
	sub $2,$3,$4
	
	li $3,9
	li $4,5
	addu $2,$3,$4
	
	li $3,38
	li $4,13
	addu $2,$3,$4
	
	li $3,73
	li $4,123
	addu $2,$3,$4
	
	li $3,35
	li $4,106
	addu $2,$3,$4
	
	li $2,97
	li $3,46
	or $4,$2,$3
	
	li $2,20
	li $3,17
	or $4,$2,$3
	
	li $2,95
	li $3,52
	or $4,$2,$3
	
	li $2,123
	li $3,2
	or $4,$2,$3
	
	li $2,99
	li $3,118
	and $4,$2,$3
	
	li $2,9
	li $3,118
	and $4,$2,$3
	
	li $2,4
	li $3,92
	and $4,$2,$3
	
	li $2,37
	li $3,120
	and $4,$2,$3
	
	li $2,50
	li $3,98
	nor $4,$2,$3
	
	li $2,79
	li $3,99
	nor $4,$2,$3
	
	li $2,54
	li $3,97
	nor $4,$2,$3
	
	li $2,19
	li $3,111
	nor $4,$2,$3
	
	li $2,47
	li $3,45
	multu $2,$3
	nop
	li $2,55
	li $3,114
	multu $2,$3
	nop
	li $2,76
	li $3,46
	multu $2,$3
	nop
	li $2,58
	li $3,62
	multu $2,$3
	nop


	.set reorder
	.end entry
