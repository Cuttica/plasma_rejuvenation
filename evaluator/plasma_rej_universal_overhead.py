from __future__ import with_statement
import time, os, re, sys, shutil, operator

yearsOfAging = 10
simtime = "12000"
assExtension = '.asm'
testExtension = ".tst"
sim_time_map = True
fixed_simtime = True
#overheads = [0, 0.0000000001, 0.000000001, 0.00000001, 0.0000001 , 0.000001 , 0.00001 , 0.0001 , 0.001, 0.01, 0.05, 0.1, 0.2, 0.5,  1, 10, 100]
overheads = [0, 0.001, 0.01, 0.1, 1, 100]
test_type_map = {
   'and.asm': 11850,
   'nor.asm': 11850,
   'or.asm': 11850,
   'xor.asm': 11850,
   '695.asm': 4000,#3750,
   '705.asm': 11850,
   '745.asm': 11850,
   '760.asm': 4000,#3000,
   '802.asm': 4000,#3150,
   'rand_1.asm': 30000,
   'rand_2.asm': 10000,
   'rand_3.asm': 4000,#3300,
   'add.asm': 7350,
   'div.asm': 54750,
   'mixed.asm': 26550,
   'mul.asm': 8700,
   'sub.asm': 7350,
   'nop.asm' : 4000,
   'worst_ugp.asm' : 4000,
   'best_0.tst' : 5850,
   'shift.asm' : 6450

}
if not ('CurDir' in vars() or 'CurDir' in globals()): CurDir = "./"


# _VECTORIZED has the same initest folder
iniTestDir = CurDir + 'wl_tests/' # qua ci metto tutti i workload da compilare. #re.sub('_VECTORIZED', '', design) + '_tests/'
#iniCompiledTestsDir = iniTestDir + 'wl_tests_compiled'
result_file = iniTestDir + "universal_progress.txt"

ugpPath = CurDir + "../"
responseFile = CurDir + "to.ugp"
requestFile = ugpPath + "individualsToEvaluate.txt" #"from.ugp"
bestIndividual = ugpPath + "BEST_pop1.s"
selectedLog = ugpPath + "evaluator.log"
bestUniversalProg = iniTestDir + 'best_0.tst'


design = 'ALU32_TB'

printf('building')

rebuild()

execfile(CurDir + "plasma_rej_eval.py")

def log(msg):
	printf(msg)

def natural_key(string_): return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]

os.path.exists(result_file) and os.remove(result_file)

def logResult(individualID):
	individualID = os.path.basename(individualID)
	with open(result_file,'a') as f:
		msg = "%s-vec %s = %s" % (clocks, individualID, delay)
		f.write(msg+"\n")
		printf("%s-th simulation, \"%s\", is over: (%s vectors). Got  D E L A Y = %s", simNo, individualID, clocks, delay)


## simulate all vectors and collect probabilities
tests = {}
def stringify(zeri): return ", ".join("%s=%s" % (i, zeri[i]) for i in zeri)

for test in os.listdir(iniTestDir):
	if test.endswith(assExtension):
		print "compiling %s" % test
		#compile(iniTestDir + test)
		if sim_time_map:
			if test in test_type_map.keys():
				simtime = str(test_type_map[test])
		simulate(iniTestDir + test,simtime)
		tests[test] = nzeri.copy() # back up the nominal workload probabilities

time.sleep(5)

def eq( a, b, eps=0.0001 ): return abs( a - b ) <= eps # / abs(a) <= eps


# adds overhead of just simulated load to the tests
def rejuvenate(title):

			overhlyHighPz = dict.fromkeys([0.9999999999978891], 0)
			def fileln(row, openkind = 'a'):
				with open(rejuvenatedResults,openkind) as f: f.write(row + "\n")
				with open(rejuvenatedResults[:-3] + 'csv',openkind) as f: f.write(row + "\n")

			rejuvenatedResults = title + "-rejuvenated.txt" ; userOvhdTable = {}
			fileln("universal rejuvenation", 'w') ; printf("created %s", rejuvenatedResults)

			def format(fnum): return "%.2f" % round(fnum,2)

			firstTest = tests.itervalues().next() # set all zeroes or ones to see theoretical best/worst cases

			json = "[\n"

			def sa(prob):
				path_t = agingAwareStatisAnalysis(dict.fromkeys(firstTest, prob))
				#title = "0time_static_analisys" if prob == 0.0 else title = "all_pz_to_1"
				if prob == 0.0:
					title = "0time_static_analisys"
				else:
					title = "all_pz_to_1"
				path = '\t{"test":"%s", "path": %s},\n' % (title,str(path_t)[:-1])
				fileln("delay when all inputs zero prob = %s is %s" % (prob, format(path_t.delay())))
				return path
			json += sa(1.0) ; json += sa(0.0)

			#with open('paths.json','a') as f: f.write("[\n")

			for user in tests:
				ohDict = {}
				json += '\t{"test":"%s","path":' % user
				for overhead in overheads:
					rejuvenated = tests[user].copy()
					#fileln("%s pz = (%s)" % (user, stringify(tests[user])))
					for c in rejuvenated:
						u_zer = rejuvenated[c]
						best_zer = nzeri[c]
						user_load = 100-overhead
						rejuvenated[c] = (u_zer * user_load + best_zer * overhead)/100

					#longestPath = recomputeLongest(rejuvenated)
					longestPath = agingAwareStatisAnalysis(rejuvenated)

					if overhead == 0: json += str(longestPath) + "\n"

					for net in rejuvenated: # we probably do not need the overhlyHighPz anymore.
						pz = rejuvenated[net] # It was used with old formula where voltage
						for ohp in overhlyHighPz: # exceeded 2.7 volts. Get rid of it.
							if pz > ohp and pz < 1.0: overhlyHighPz[ohp] += 1

					#fileln("  %s%%: del = %s, pz = %s" % (overhead, longestPath.delay(), stringify(rejuvenated)))

					assert len(rejuvenated) == len(nzeri)
					if overhead == 100: # checks
						for i in rejuvenated: assert eq(rejuvenated[i], nzeri[i]), "rejuvenated[%s] is %s != bestZeri[%s] = %s" % (i, rejuvenated[i], i, nzeri[i])
						if 'bestTracker' in 'bestTracker' in globals(): assert eq(longestPath.delay(), bestTracker.delay), "recomputed longest path delay %s mismatches the best delay %s " % (longestPath.delay(), bestTracker.delay)

					# if prob > doubleAgingProb
					d = {} ; d["delay"] = "%s" % format(longestPath.delay())
					#if "-probs" in arg2: d["probs"] = " ".join(str(rejuvenated[i]) for i in rejuvenated).replace(".0 "," ")
					'''if "-zcnt" in arg2:
						lpz = longestPath.parent().fold((), lambda acc, path: acc + (rejuvenated[path.computedNet.net],))
						def static(pzets): return sum(pz > doubleAgingProb for pz in pzets)
						# reports static zeroes at critical path, length of the critical path and total number of static pz in the circuit
					'''#	d["zcnt"] = "%s/%s/%s" % (static(lpz), len(lpz), static(rejuvenated.values()))
					ohDict[overhead] = d

				userOvhdTable[user] = ohDict

				json = json[:-2]
				json += '},\n'

			json = json[:-2]

			def appendTable(ttitle):
				sortedTests = [test for test in sorted(tests, key=natural_key)]
				def line(ltitle, values): fileln(ltitle + "," + ",".join(map(lambda tst: re.sub('\.tst$', '', tst), values)))
				line("\n" + ttitle, sortedTests)
				for oh in overheads: line("%s%%" % oh, [userOvhdTable[user][oh][ttitle] for user in sortedTests])

			appendTable("delay")# ; [appendTable(key) for key in ["zcnt", "probs"] if "-"+key in arg2]

			with open("paths.json",'w') as f:
				f.write("%s\n]" % (json))

			for ohp in overhlyHighPz:
				fileln("there are %s nets with pz in range (%.16f, 1)" % (overhlyHighPz[ohp], ohp))


if justSimulation:
	if sim_time_map:
		#simtime = '10200'#<-- best plasma #'4650' <-- control
		simtime = '4000'#<-- best plasma #'4650' <-- control
	simulate(bestUniversalProg,simtime)
	rejuvenate(bestUniversalProg)
	os._exit(0)



if int(arg) > 0:

	def writeResponse(response):
		print "responding " + response
		with open (responseFile, 'w') as f: f.write (response)

	def run_uGP():
		'''
		def replace(replacement):
			sfile = ugpPath + "population.settings.xml"
			with open(sfile,'r') as f: settings = f.read()
			for r in ["12-bit", "5-bit"]: settings = settings.replace(r, str(replacement) + "-bit")
			with open(sfile,'w') as f: f.write(settings)

		if "C17" in design: replace(5)
		if "ALU4" in design: replace(12)
		'''
		#os.system("cd .. & add_tests " + str(testsToAdd))
		sys.path.append(CurDir)
		print '\''+ugpPath+'\''
		os.system('start cmd /K "cd '+ugpPath+' & pwd & add_tests '+arg+'"')

	import threading
	threading.Thread(target=run_uGP).start()

	#from subprocess import Popen; Popen(['cmd', '/c pause'])

	writeResponse("0")

	log ("Evaluator: waiting for vectors in " + requestFile + " and producing output in " + responseFile)

for bestID in range(int(arg)):


	# used for checking the best individual
	class BestTracker:
		def __init__(self, fitness): self.fitness = fitness ; self.score = 0
		def update(self, delay):
			score = self.fitness(delay)
			if (score > self.score): self.delay = delay; self.score = score
			return score

	def worstFitness(delay):
		with open(ugpPath + path) as f: lines = f.readlines(); lack = len(lines) - len(set(lines)); printf("worst fitness = %s * %s", delay, 2**(-lack)); return delay*2**(-lack)

	bestTracker = BestTracker(worstFitness) if worst_fitness else BestTracker(fitness)
	#bestTracker = BestTracker(fitness)

	while True:

		time.sleep (.2)
		if os.path.exists(bestIndividual):
			bestPrefix = iniTestDir + "best_" + str(bestID)
			newTest =  bestPrefix + testExtension
			#newTest = iniTestDir + "best_" + str(len(executedAssignments)) + testExtension
			ensureRemove(newTest) if os.path.exists(newTest) else None
			while (True): # the fact that uGP has created the best file does not mean that it is complete. It is still writing.
				try: os.rename(bestIndividual, newTest) ; break
				except: print "failed to rename the best %s -> %s, retrying" % (bestIndividual, newTest) ; time.sleep (.2)
			delay = simulate(newTest,simtime) # simulate once more to recall the rating of the recorded individual
			print 'bestTracker: '+ str(bestTracker.update(delay))
			assert eq(bestTracker.delay, delay), "simulation reproduced %s whereas it must be %s" % (delay, bestTracker.delay)
			log("We have terminated because " + bestIndividual + " exists. We grabbed it => "
				+ newTest + " after "+str(simNo)+" simulations. DELAY = " + str(delay))
			#writeSelected(str(simNo) +  "th test was selected with " + str(simNo) + " simulations , " + fitness)
			simNo = 0
			rejuvenate(bestPrefix)
			break

		if not os.path.exists(responseFile): # uGP has consumed our evaluation. It means that it has supplied a new individual already
			with open (requestFile, 'r') as f: path = f.readline().strip()
			#executedBefore = testSize # we want to rollback executed later

			#added 7/6/2016
			n_alu_instructions = 0
			with open(ugpPath + path, "r") as f1:
				for line in f1.readlines():
					line = line.strip().lower()
					if "add" in line[:3] or "sub" in line[:3] or "bg" in line[:2] or "bl" in line[:2] or "beq " in line[:4] or "bn" in line[:2] or "or" in line[:2] or "and" in line[:3] or "xor" in line[:3] or "nor" in line[:3] or "nop" in line[:3]:
						#instruction that interest the ALU -> count ++
						n_alu_instructions += 1

			delay = simulate(ugpPath + path,simtime)
			writeResponse("%s %s" % (bestTracker.update(delay), str(n_alu_instructions))) ; logResult(path)

			print("																		FITNESS  -> %s" % bestTracker.update(delay))
			print("																		FITNESS  -> %s" % bestTracker.update(delay))
			print("																		FITNESS  -> %s" % bestTracker.update(delay))
			print("																		FITNESS  -> %s" % bestTracker.update(delay))

			ensureRemove(requestFile) # signal that we have finished writing output
			#testSize = executedBefore # we want to rollback executed later
