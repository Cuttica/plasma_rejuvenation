from Tkinter import *
import json

canvas_width = 4000 #1200
canvas_height = 1000
signal_file = 'all_signals.txt'
paths_file = 'paths.json'
margin = 15
h_width = canvas_width - margin
h_height = canvas_height - margin
line_size = 3 #1

def get_sig_list(file_name):
    with open(file_name, 'r') as f: signals = f.read()
    return [ tmp.strip() for tmp in signals.split(',')]


def get_data(file_name):
    with open(file_name, 'r') as f: mat = f.read()
    data = json.loads(mat)
    #print data
    #data elaboration:
    tmp_max = 0
    for j,test in enumerate(data):
        path =test['path']
        par_m = 0
        for i, sig in enumerate(path):
            if i != 0: sig['delay'] -= path[i-1]['delay']
            if par_m < sig['delay'] : par_m = sig['delay']
            if tmp_max < sig['delay']: tmp_max = sig['delay']
            #print sig['name'] + " " + str(sig['delay'])
        print str(tmp_max) + " t:" + str(par_m)
    return data, tmp_max


sig_list = get_sig_list(signal_file)
data,max_value =get_data(paths_file)
master = Tk()


def callback(show_test):
    #print show_test
    for can in canvas_list.values(): can.pack_forget()#.itemconfigure("highlights",state='hidden')
    canvas_list[show_test].pack()

def create_button(name):
    b = Button(buttons_frame,text=name, command=lambda : callback(name))
    b.pack(side = LEFT)
    return b

def create_canvas(path,j):
    c = Canvas(master, width=canvas_width, height=canvas_height)
    c.pack()

    x_axis = c.create_line(margin,h_height,h_width,h_height)
    y_axis = c.create_line(margin,h_height,margin,margin)

    delta_x =  (h_width -  (line_size)*len(sig_list))  /   len(sig_list)
    print 'delta= ' + str(delta_x)
    #print 'delta = ' + str(delta_x) + 'n = ' + str(len(sig_list))
    #colors = ["red", "orange", "yellow", "green", "blue", "violet"]
    colors = [
        (36, 0, 216), (24, 28, 247), (40, 87, 255), (61, 135, 255), (86, 176, 255),
        (255, 172, 117), (255, 120, 86), (255, 61, 61), (247, 39, 53), (216, 21, 47), (165, 0, 33)
              ]
    colors = ["#%02x%02x%02x" % ele for ele in colors]
    alfa = h_height / max_value
    #color = colors[j % len(colors)]
    for i,s in enumerate(sig_list):
        #print "i = " + str(i) + ',   sig: ' + s
        x0 = x1 = ( (delta_x+ line_size) * (i + 1) ) + margin
        y0 = h_height
        def get_normalized_delay():
            for sig in path:
                if sig['name'] == s:
                    beta = sig['delay'] / max_value
                    index = int(beta * len(colors))-1
                    if index < 0: index = 0
                    color = colors[index]
                    color = 'black'
                    return h_height - (sig['delay'] * alfa), color
            return h_height+10, colors[0]

        y1,color = get_normalized_delay()
        c.create_line(x0,y0,x1,y1,fill=color, width= line_size)
    return c

buttons = [None] *len(data)
buttons_frame = Frame(master)
buttons_frame.pack()
canvas_list = {}
for i in range(len(data)):
    buttons[i] = create_button(data[i]['test'])


for i,item in enumerate(data):
    canvas_list[item['test']] = create_canvas(item['path'], i)

for i,can in enumerate(canvas_list.values()):
    if i !=0: can.pack_forget()

mainloop()

