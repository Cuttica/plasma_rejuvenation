import os
import re
import random
import sys
import math

asm_folder = "./wl_tests/"

res_folder = "./compilingTest/asm_test/"

def get_random(max, min=0):
    return random.randint(min, math.pow(2,max-1))

regex = re.compile(r'(?<=[^$\d])\d+(?!\d*[(:])', re.IGNORECASE)

for file in os.listdir(asm_folder):
    print "file:" + file

    for k in [16, 24, 32]:
        with open(asm_folder + file, "r") as f:
            #text = f.read()
            mod = ""
            line = f.readline()
            while line != "":
                if regex.search(line) is not None:
                    if "x" in line or "B" in line or "syscall" in line or "break" in line or "J" in line or "#" in line or ".align" in line:
                        mod += line
                    else:
                        line = re.sub(r'(\d+\*\d+)|(\d+-\d+)',str(get_random(k)),line)
                        r = regex.sub(str(get_random(k)), line)
                        mod+=r
                        print " " + line[:-1] + "       " + r
                else:
                    mod += line
                line = f.readline()

            with open(res_folder + file[:-4] + "_" + str(k) + ".asm", "w") as f2:
                f2.write(mod)
