from __future__ import division
from __future__ import with_statement
from org.zamia.instgraph import IGObject
from org.zamia.instgraph.IGObject import IGObjectCat
from org.zamia.instgraph.IGType import TypeCat
from sets import Set
import shutil
import org.zamia.instgraph.IGOperationIndex
import org.zamia.instgraph.IGInstantiation
import random

maxpathsToTrack = 60 # sys.maxint for no limit
doubleAgingProb, doublingFactor = 0.99999999, 2 # Pz = 1 switches degrade twice as much


gates, pi, po = Set([]), Set([]), Set([])

nzeri = {} # counts zeroes at gate inputs
drivers = {} # maps nets to gates
'''
tl = Toplevel(DMUID.parse("WORK." + design), None)
module = project.getIGM().findModule(tl)
printf("design name=%s, top=%s", design, module)
dut_gates = module.findChild("DUT_GATES")
printf("top=%s, DUT_GATES = %s", module, dut_gates)
module = project.getIGM().findModule(dut_gates.getSignature())
'''
tl = Toplevel(DMUID.parse("WORK.TBENCH"),None)
module = project.getIGM().findModule(tl)
dut_gates = module.findChild("U1_PLASMA")
print str(dut_gates.getSignature())
dut_gates = dut_gates.findChild("U1_CPU")
print str(dut_gates.getSignature())
dut_gates = dut_gates.findChild("U6_ALU")
print str(dut_gates.getSignature())
#os._exit(0)
module = project.getIGM().findModule(dut_gates.getSignature())
printf("reached WORK.TBENCH:U1_PLASMA.U1_CPU.U6_ALU")

class Gate:

	def label(self):
		return str(self.smt.getLabel())

	def type(self):
		return str(self.smt.getChildDUUID().getId())

	def inputs(self):
		inputs = []
		for i in range(0, self.smt.getNumMappings()):
			def id(igOpObject):
				if isinstance(igOpObject, org.zamia.instgraph.IGOperationIndex):
					#printf("igobject " + igOpObject.getOperand().getObject().getId() + "(" + str(igOpObject.getIndex()) + ")")
					return igOpObject.getOperand().getObject().getId() + "(" + str(igOpObject.getIndex()) + ")"
				return igOpObject.getObject().getId()
			m = self.smt.getMapping(i) ; formal = id(m.getFormal())
			#print "mapping(" + str(i) + "): " + str(m) + ", formal: " + str(formal)
			if formal != 'Z':
				inputs.append((id(m.getActual()), formal))
			else:
				self.out = id(m.getActual())
				#printf ("%s drives %s" % (self, self.out))

		#printf("gate %s(%s), inputs=(%s), out=%s", self.label(), self.type(), inputs, self.out)
		return inputs

	def __init__(self, igSmt):
		self.smt = igSmt
		for i,_ in self.inputs():
			nzeri[i] = 0 # initialize counters with dummy values. This must be done in every simulation anyway.
			pi.add(i)

		drivers[self.out] = self
		self.load = 0
		po.add(self.out) # we first add all outputs into po and then we'll remove thse which are inputs of other gates


	def __str__(self): return self.label()

for smt in module.getStructure().getStatements():
	if isinstance(smt, org.zamia.instgraph.IGInstantiation):
		gates.add(Gate(smt))

#norman invented that primary inputs are signals, which are not driven by gates
pi -= Set(drivers.keys()) # signals not driven by gates -- must be primary input
po -= Set(nzeri.keys())

# compute fan-outs
for g in gates:
	for i,_ in g.inputs():
		if not i in pi: drivers[i].load += 1

for prin in pi: print "<<<< pi: "+ str(prin)
for driver in drivers: print ">>>>>>>driver " + str(driver)
	#printf ("%s loads %s over %s" %(g, drivers[i], i))

#for l in module.getContainer().localItems():
#	if isinstance(l, org.zamia.instgraph.IGObject) and l.getCat() == IGObject.IGObjectCat.SIGNAL:
#		sig = l.getId()
#		if not sig in pi:
#			printf("%s has signal %s, driven by %s" % (module, sig, driver(sig)))
#
#	else: logger.error("unkown local %s in module %s", l, module)


def processARange(obj, processor, value = None):
	loc = obj.computeSourceLocation()
	iType = obj.getType().getStaticIndexType(loc)
	#printf("processing %s", obj.getId())
	low = iType.getStaticLow(loc).getInt()
	high = iType.getStaticHigh(loc).getInt()
	asc = iType.isAscending()
	for i in range(low, iType.getStaticHigh(loc).getInt()+1):
		id = "%s(%s)" % (obj.getId(), i)
		processor(id, None if value == None else value[i-low if asc else high-i])

# What is the fanout of primary output? Let's make it 1
for l in module.getContainer().interfaces():
	if isinstance(l, org.zamia.instgraph.IGObject) and l.getCat() == IGObjectCat.SIGNAL:

		def load(sig):
			printf("interface %s", sig)
			if not sig in pi: drivers[sig].load += 1

		type = l.getType() # convert arrays into a set of id(index) nets
		if (type.getCat() == TypeCat.ARRAY):
			processARange(l, lambda id, ignored: load(id))
		else:
			load(l.getId())
	else: logger.error("unkown interface %s in module %s", l, module)

for g in gates:
	if g.load < 1: logger.warn(" = = = = = = = = WARNING: gate %s HAS NO LOAD!!!= = = = = = = = = " % g)
	#printf ("%s fanout = %s" % (g, g.load))


'''
						'ND2' : ({'A': (1.5, 4.44), 'B': (2.15, 1.63)}, (gate_inputs + 2)/3),
						'NR2' : ({'A': (2.3383, 4.5987), 'B': (2.0521, 7.943)}, (2*gate_inputs + 1)/3),
						'IV' : ({'A': (1.64, 6.76)}, 1)
'''
 #This is the new formula, for now i will use the older one
'''
# Gate load was necessary only to compute nominal delay.
# Later on, for aging delay computation, we'll need nominal delays instead of load. Convert load => nominal delay
#gtypes = dict(((g.type(), g.load), g.out) for g in gates if g.load < 4) ; printf("gate types = %s" % gtypes)
gateParamsTable = {}
for gate in gates:
	gate_inputs = len(gate.inputs()) ; (lm, logic_effort) = {
						'ND2' : ({'A': (.96, 2.45), 'B': (1.1, 1.75)}, (gate_inputs + 2)/3),
						'NR2' : ({'A': (1.16, 2.92), 'B': (.82, 4.65)}, (2*gate_inputs + 1)/3),
						'IV' : ({'A': (.7, 3.2)}, 1)
					}[gate.type()]
	nominal_delay = logic_effort * gate.load + gate_inputs
	gateParamsTable[gate.type()] = lm ; gate.load = nominal_delay

(alfa, beta) = ({0: 0, 1: 15, 10: 115 }[yearsOfAging] * 10**(-7), 0.18868) ; oneOver18 = 1.0/18
#v = 0.37 if pz == 1.0 else (al * pz)**be if pz < 0.5 else 0.101+(0.66-((1-pz)/1000)**oneOver18)/3

Kv = 6.90971899731047E-07 ; C = 29.4810987290415 ; t =10*365*24*60*60
n = 1.0/6; tox = 1.2 ; nKvCt_over_810tox = n **2 * Kv **2 * C *t/ (810 * tox ** 2 )
dVmax_expN = .27 ** (1.0/n) ; pz_max= dVmax_expN / (nKvCt_over_810tox   + dVmax_expN)

# Single gate delay
def gateDelay(gate, pz, input):

#	Vth = (alfa * pz)**beta ; if pz > doubleAgingProb: Vth *= doublingFactor

	#emperical fit
	##Vth = (alfa * pz)**beta if pz < 0.5 else .27 if pz > 0.9999999999978891 else 0.101+(0.66-((1-pz)/1000)**oneOver18)/3
	#Vth = (alfa * pz)**beta if pz < 0.5 else 0.101+(0.66-((1-pz * 0.9999999999978892)/1000)**oneOver18)/3

	#physically-backed model
	#if pz > pz_max: pz = pz_max
	#Vth = ( nKvCt_over_810tox * pz/ (1-pz)) ** n
	pz = pz * pz_max ; Vth = ( nKvCt_over_810tox * pz/ (1-pz)) ** n

	#printf("gate = %s(%s), pz %s => Vth %s", str(gate), gate.type(), pz, Vth)

	(lamda, mu) = gateParamsTable[gate.type()][input]

	#printf(u"logic_effort * gate.load + #inputs=%s, (l,m)=%s" , gate.load, lm[input])

	dt = Vth * (lamda  + mu * Vth)

	# gate.load actually contains (logic_effort * gate.load + gate_inputs)
	return gate.load * (1 + dt)

	#Fi = 1.28 ; result *= (1 + Fi * (fanout - 1)/100)
'''
# This is the older formula
gateParamsTable = {}
for gate in gates:
	gate_inputs = len(gate.inputs()) ; (lm, logic_effort) = {
						'ND2' : ({'A': (1.33, 5.75), 'B': (1.38, 7.5)}, (gate_inputs + 2)/3),
						'NR2' : ({'A': (1.53, 10.35), 'B': (1.53, 11.40)}, (2*gate_inputs + 1)/3),
						'IV' : ({'A': (1.63, 5.3)}, 1)
					}[gate.type()]
	nominal_delay = logic_effort * gate.load + gate_inputs
	gateParamsTable[gate.type()] = lm ; gate.load = nominal_delay

(alfa, beta) = ({0: 0, 1: 15, 10: 115 }[yearsOfAging] * 10**(-7), 0.18868) ; oneOver18 = 1.0/18
#v = 0.37 if pz == 1.0 else (al * pz)**be if pz < 0.5 else 0.101+(0.66-((1-pz)/1000)**oneOver18)/3

def gateDelay(gate, pz, input):

#	Vth = (alfa * pz)**beta ; if pz > doubleAgingProb: Vth *= doublingFactor

	Vth = (alfa * pz)**beta if pz < 0.5 else 0.101+(0.66-((1-pz * 0.9999999999978892)/1000)**oneOver18)/3
	#Vth = (alfa * pz)**beta if pz < 0.5 else .27 if pz > 0.9999999999978891 else 0.101+(0.66-((1-pz)/1000)**oneOver18)/3

	#printf("gate = %s(%s), pz %s => Vth %s", str(gate), gate.type(), pz, Vth)

	lm = gateParamsTable[gate.type()] ; (lamda, mu) = lm[input]

	#printf(u"logic_effort * gate.load + #inputs=%s, (l,m)=%s" , gate.load, lm[input])

	dt = Vth * (lamda  + mu * Vth)

	# gate.load actually contains (logic_effort * gate.load + gate_inputs)
	return gate.load * (1 + dt)

	#Fi = 1.28 ; result *= (1 + Fi * (fanout - 1)/100)
# Path delays
class Origin(object): # path from primary pin to primary pin
	def __init__(self, net, delay = 0):
		self.net = net
		self.switchDelay = delay

	def delay(self): return 0

	def __str__(self): return str(self.net)


class Expansion(Origin): # element of a path

	def __init__(self, net, parent, delay):
		self.parent = parent
		super(self.__class__, self).__init__(net, delay)

	def delay(self): return self.parent.delay() + self.switchDelay

	def __str__(self): return "%s  +%s=%s  %s" % (self.parent, self.switchDelay, self.delay(), self.net)

gatesToProcess = Set(gates)

# maps net to a set of (critical) paths that lead to it. Initialized to primary inputs.
processedNets = dict([(net, [Origin(net)]) for net in pi])

# Here we comment out the old approach where only longest paths were considered
#for pp in processedNets: printf ("processed %s -> %s" % (pp, ",".join([str(i) for i in processedNets[pp]])))

#def dropPaths(paths, n):
#	s = sorted(paths, key=lambda student: student.delay())
#	return s[-n:] # leave last n items
#
#
#while len(gatesToProcess) > 0:
#	newSchedule = Set()
#	for gate in gatesToProcess:
#		ginputs = gate.inputs()
#		canProcess = all(gi in processedNets.keys() for gi in ginputs)
#		#printf("%s can process %s", gate.label(), canProcess)
#		if canProcess:
#			processedNets[gate.out] = []
#			for gi in ginputs:
#
#				for pp in processedNets[gi]:
#					expansion = Expansion(gate.out, pp, gateDelay(gate, 0, 0))
#					processedNets[gate.out].append(expansion)
#					#printf("  got %s for gate %s" % (expansion, gate))
#
#				#TODO: remove too short paths
#				processedNets[gate.out] = dropPaths(processedNets[gate.out], maxpathsToTrack)
#
#
#		else:
#			newSchedule.add(gate)
#	gatesToProcess = newSchedule
#
#
#tracedPaths = []
#for pn in processedNets:
#	if pn in po: tracedPaths += processedNets[pn]
#tracedPaths = dropPaths(tracedPaths, maxpathsToTrack)
#
##printf("%s", "\n".join("path to " + path.net + ": " + str(path) for path in tracedPaths))
#printf("Tracking %s static paths, longest is %s" % (len(tracedPaths), dropPaths(tracedPaths, 1)[0]))
#
#
#def recomputeLongest(nzeri):
#
#	dynamicPaths = []
#	for path in tracedPaths:
#
#		p, pathNets = path, [] # backtrack to the origin
#		while isinstance(p, Expansion):
#			pathNets.insert(0, p.net)
#			p = p.parent
#
#		# wrong dynamic delays -- we must alternate presence of aging
#		pFalling = pRaising = pNoAging =  pBoth = p # p is currently set to primary input
#		falling = True
#		for net in pathNets:
#			gate = drivers[net]
#			def expand(p, agingOn):
#				delay = gateDelay(gate, nzeri[p.net]  if agingOn else 0)
#				#printf(" delay " + "%s (nzeri=%s)" % (p.net, nzeri[p.net]) + "->" + net + " = " + str(delay))
#				return Expansion(net, p, delay)
#
#			pBoth = expand(pBoth, True)
#			pFalling = expand(pFalling, falling)
#			pRaising = expand(pRaising, not falling)
#			pNoAging = expand(pNoAging, False)
#			falling = not falling
#
#		dynamicPaths.append(pFalling)
#		dynamicPaths.append(pRaising)
#		assert pNoAging.delay() == pNoAging.delay(), "Reproducing static analysis aging (%s) reproduced a different delay (%s)" % (path, pNoAging)
#		#printf("Recomputed static %s\n input \\:  %s\n input /:  %s\n  /\\:  %s" % (path, pFalling, pRaising, pBoth))
#
#
#	#delays = map(lambda p : p.delay(), dynamicPaths) ; return max(delays)
#
#	#longestPath = reduce(lambda acc, p2: acc if acc.delay() > p2.delay() else p2, dynamicPaths)
#
#	result = dropPaths(dynamicPaths, 1)[0]
#	alternative = agingAwareStatisAnalysis(nzeri)
#	assert result.delay() == alternative.delay(), "Traced path delay %s mismatches aging-aware SA %s. \n Traced path = %s\n SA path = %s" % (result.delay(), alternative.delay(), result, alternative)
#	return result



# simulate -- aged delays

from org.zamia.instgraph.sim.ref import SimData

clk = PathName("CLK")

simNo, clocks = 0, None

def fitness(delay):
	if random_tests: return (random.random() + 0.1 ) * 100
	return 1000/delay

def ttimeit(title, f):
	from timeit import default_timer as timer
	start = timer() ; result = f() ; print(title + " time = " + str(timer() - start) + " sec")
	return result

def simulate(path,simtime): return ttimeit("simulation", lambda: simulate1(path,simtime))

def simulate1(path,simtime):
	global simNo, clocks

	clocks = 0
	simNo += 1
	for i in nzeri: nzeri[i] = 0

	printf("simulating %s", path)
	#shutil.copy2(path, CurDir + "STIMULI")
	compile(path)
	'''''
	if not fixed_simtime:
		header = True
		nlines = 0
		with open('./code.txt', 'r') as f:
			for line in reversed(f.readlines()):
				# print(line)
				line = line.strip()
				if line != "":
					if line != '00000000' or not header:
						# print line + " " + str(header)
						header = False
						nlines += 1
		simtime = str(nlines * 150 + 2850)  # simtime set to number of lines in assembled file * clk_period

		print "===================    " + " nline: " + str(nlines) + " ---- " + simtime + "     ================="
	'''''
	class ClkMonitor(SimData.TraceAddListener):

		def trace(self, SignalPath, time, Value, IsEvent):

			global clocks

			if IsEvent and clk.equals(SignalPath):
				if time >= 2850000000:
					clocks += 1
					#print "-------------------------------------------- tempo: " + str(time) + "--------------------------------------------"
					for l in module.getContainer().localItems():
						if isinstance(l, org.zamia.instgraph.IGObject) and l.getCat() == IGObjectCat.SIGNAL:

							def count(id, value):
								if not id in nzeri:
									#logger.warn("ignored %s. PO?", id)
									assert id in po
									return
								if value == '0': nzeri[id] += 1
								#printf("clock %s: %s = %s, cnt => %s" % (clocks, id, value, nzeri[id]))

							id = l.getId()

							value = str(sim.getValue(PathName("U1_PLASMA.U1_CPU.U6_ALU." + id)))
							if len(value) == 1:
								count(id, value)
							else:
								#printf("%s too long value = %s" % (id, value))
								processARange(l, lambda id, bitvalue: count(id, bitvalue), value)


	#sim = openSim()
	sim = openSim2(tl.getDUUID())

	sim.fData.traceAddListeners.add(ClkMonitor())

	run(sim, simtime)


	# recompute the paths
	for i in nzeri: nzeri[i] /= clocks

	#printf("Pzs: " + ",".join("%s=%s" % (i, nzeri[i]) for i in sorted(nzeri.keys())))

	#longestPath = recomputeLongest(nzeri)
	longestPath = agingAwareStatisAnalysis(nzeri)
	#printf("longest = %s" % longestPath)

	return longestPath.delay()

# Gate leveling for faster static analysis.
gateLevels = [] ; processedNets = Set(net for net in pi) ; gatesToProcess = Set(gates)
'''
Il seguente pezzo di codice serve per dividere i vari gates in livelli, il primo livello contiene quei gate che hanno come input solo i primary input,
il secondo i primary input e/o gli output del primo livello, i secondi i pi e/o gli output del primo o del secondo livello e cosi via...
'''
while len(gatesToProcess) > 0:
	gateLevels.append(Set()); postponed = Set()
	for gate in gatesToProcess:
		#caProcess contiene un valore booleano: True se tutti gli input di gate sono nell'insieme processedNets, False altrimenti
		canProcess = all(gi in processedNets for gi,_ in gate.inputs())
		#printf("%s can process %s at level %s", gate.label(), canProcess, len(gateLevels))
		if canProcess: gateLevels[-1].add(gate) #se si aggiungiamo al livello corrente il nuovo gate
		else: postponed.add(gate) #altrimenti scartiamo lo mettiamo tra quelli ancora da analizzare
	gatesToProcess = postponed
	for g in gateLevels[-1]: processedNets.add(g.out) #agiungiamo a quelli processati anche gli output di quelli appena aggiunti nel livello corrente

#def gateToStr(g): return g.type() + "(" + g.label() + ": " + (", ".join([formal + "->" + actual for actual,formal in gate.inputs()])) + ")=>" + g.out
#def gateToStr(g): inputs = dict(g.inputs()) ; return for a in inputs
#for gl in gateLevels: printf( "level %s" % map(gateToStr, gl))
#for gl in gateLevels: printf( "level %s" % map(lambda g: g.label(), gl))

def agingAwareStatisAnalysis(nzeri): return ttimeit("timing analysis", lambda: agingAwareStatisAnalysis1(nzeri))

def agingAwareStatisAnalysis1(nzeri):

	class ComputedNet(object): # path from primary pin to primary pin

		# first el of rising/falling tuple is parent ComputedNet, second is the delay
		def __init__(self, net, r = (None, 0), f = (None, 0)):
			self.net = net
			self.r = r
			self.f = f

		def visit(self, path, rootf, extraf): return rootf(path)

		def __str__(self): return self.net

	class ExtraNet(ComputedNet): # path from primary pin to primary pin
		def __init__(self, net, r, f):
			super(self.__class__, self).__init__(net, r, f)
			#printf("constructing %s" % self)

		def visit(self, path, rootf, extraf):
			return extraf(path, path.parent().visit(rootf, extraf))

		def __str__(self):
			def format(tuple): return "(%s:%s)" % (tuple[0].net, tuple[1])
			return "%s from %s and %s" % (self.net, format(self.r), format(self.f))
		#def __str__(self): return "%s r:%s, f:%s" % (self.net, self.r, self.f)


	'''
	Il seguente pezzo di codice serve calcolare i dalay dei percorsi tra i gates, alla posizione dell'id della net c'e' una lista  di net e delay... si possono stampare tutti i percorsi
	'''
	# maps net to a set of (critical) paths that lead to it. Initialized to primary inputs.
	processedNets = dict([(net, ComputedNet(net)) for net in pi])

	for gl in gateLevels:
		for gate in gl:
			ginputs = gate.inputs()
			def pickWorst(edge): # edge is rising or falling
				worst = (None, -1) # dummy beyound worst
				for gi,formal in ginputs:
					prob = nzeri[gi] if edge == 'r' else 0
					delay = gateDelay(gate, prob, formal)
					#printf("  %s %s[%s, %s] -> %s = %s" % (gate.type(), gi, formal, prob, gate.out, delay))
					delay += getattr(processedNets[gi], edge)[1]
					if delay > worst[1]: worst = (processedNets[gi], delay)
				return worst

			processedNets[gate.out] = ExtraNet(gate.out, pickWorst('f'), pickWorst('r'))

	## Now, PI-to-PO paths are computed. Take the longest one.
	class Path:
		def __init__(self, computedNet, edge):
			self.computedNet = computedNet
			self.edge = edge

		def parent(self):
			branch = getattr(self.computedNet, self.edge)
			return Path(branch[0], {'r': 'f', 'f': 'r'}[self.edge])

		def visit(self, rootf, extf): return self.computedNet.visit(self, rootf, extf)
		def fold(self, initial, f): return self.visit(lambda path: f(initial, path), lambda path, a: f(a, path))


		def __str__old(self):
			def rootf(path): return "%s%s" % (path.edge, path.computedNet.net)
			def extf(path, prefix): cn = path.computedNet; return prefix + " + %s%s=%s" % (path.edge, cn.net, getattr(cn, path.edge)[1])
			return self.visit(rootf, extf)

		def __str__(self):
			def rootf(path): return '\t[ {"type" : "%s", "name" : "%s", "delay" : 0}' % (path.edge, path.computedNet.net)  # return "%s%s" % (path.edge, path.computedNet.net)
			def extf(path, prefix): cn = path.computedNet; return prefix + ', {"type" : "%s", "name" : "%s", "delay" : %s}' % (path.edge, cn.net, getattr(cn, path.edge)[1])  # return prefix + " + %s%s=%s" % (path.edge, cn.net, getattr(cn, path.edge)[1])
			return self.visit(rootf, extf) + "],"  # self.visit(rootf, extf)

		def delay(self): return getattr(self.computedNet, self.edge)[1]

	worst = ComputedNet(None, r = (None, -1)); worst = Path(worst, 'r') # dummy worst case
	for pn in processedNets:
		if pn in po:
			computedO = processedNets[pn]
			#printf ("considering performance of %s => %s" % (pn, computedO))
			for edge in ['r', 'f']:
				candidate = getattr(computedO, edge)
				if (candidate[1] > worst.delay()):
					worst = Path(computedO, edge)
					#printf ("created Path %s" % worst)

	return worst

