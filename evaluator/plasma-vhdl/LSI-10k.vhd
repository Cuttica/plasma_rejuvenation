library IEEE; use IEEE.std_logic_1164.all;
entity AO2 is
	port( A, B, C, D : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AO2 is
begin
	Z <= ((not A or not B) and (not C or not D));
end architecture;

library IEEE; use IEEE.std_logic_1164.all;
entity IV is
  port( A : in std_logic;  Z : out 
std_logic);
end entity;
architecture LOGIC of IV is begin
	Z <= not A;
end architecture;


library IEEE; use IEEE.std_logic_1164.all;
entity AO3 is
	port( A, B, C, D : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AO3 is
begin
	z <= ((not A and not B) or not C or not D) ;
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity AO4 is
  port( A, B, C, D : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AO4 is begin
	Z <= ((not A and not B) or (not C and not D));
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity AO6 is
  port( A, B, C : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AO6 is begin
	z <= ((not A or not B) and not C) ;
end architecture;

library IEEE; use IEEE.std_logic_1164.all;
entity AO7 is
	port( A, B, C : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AO7 is
begin
	Z <= ((not A and not B) or not C);
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity ND2 is
  port( A, B : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of ND2 is begin
	Z <= (not A or not B) ;
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity ND3 is
  port( A, B, C : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of ND3 is begin
	z <= (not A or not B or not C) ;
end architecture;

library IEEE; use IEEE.std_logic_1164.all;
entity AN3 is
	port( A, B, C : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of AN3 is
begin
	Z <= A and B and C;
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity NR2 is
  port( A, B : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of NR2 is begin
	z <= (not A and not B);
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity NR3 is
  port( A, B, C : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of NR3 is begin
	z <= (not A and not B and not C);
end architecture;

library IEEE; use IEEE.std_logic_1164.all; 
entity NR4 is
  port( A, B, C, D : in std_logic;  Z : out std_logic);
end entity;
architecture LOGIC of NR4 is begin
	Z <= (not A and not B and not C and not D);
end architecture;

library CLASS;
entity EE is end entity;
architecture L of EE is begin
   U1061 : entity CLASS.ND2 port map( A => '1', B => '1', Z => open);
end architecture; 