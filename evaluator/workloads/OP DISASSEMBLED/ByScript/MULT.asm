   ori   $2,$0,'g'
   sb    $2,0($20)
   ori   $2,$0,5
   ori   $3,$0,13
   mult  $2,$3
   nop
   mflo  $4
   sb    $4,0($20)    #A
   li    $2,-5
   ori   $3,$0,13
   mult  $2,$3
   mfhi  $5
   mflo  $4
   sub   $4,$0,$4
   addu  $4,$4,$5
   addi  $4,$4,2
   sb    $4,0($20)    #B
   ori   $2,$0,5
   li    $3,-13
   mult  $2,$3
   mfhi  $5
   mflo  $4
   sub   $4,$0,$4
   addu  $4,$4,$5
   addi  $4,$4,3
   sb    $4,0($20)    #C
   li    $2,-5
   li    $3,-13
   mult  $2,$3
   mfhi  $5
   mflo  $4
   addu  $4,$4,$5
   addi  $4,$4,3
   sb    $4,0($20)    #D
   lui   $4,0xfe98
   ori   $4,$4,0x62e5
   lui   $5,0x6
   ori   $5,0x8db8
   mult  $4,$5
   mfhi  $6
   addiu $7,$6,2356+1+'E' #E
   sb    $7,0($20)
   sb    $23,0($20)
   sb    $21,0($20)

