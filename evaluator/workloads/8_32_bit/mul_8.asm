	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	li $2,120
	li $3,108
	multu $2,$3
	nop
	li $2,94
	li $3,87
	multu $2,$3
	nop
	li $2,67
	li $3,59
	multu $2,$3
	nop
	li $2,125
	li $3,26
	multu $2,$3
	nop
	li $2,49
	li $3,96
	multu $2,$3
	nop
	li $2,28
	li $3,111
	multu $2,$3
	nop
	li $2,113
	li $3,78
	multu $2,$3
	nop
	li $2,117
	li $3,108
	multu $2,$3
	nop
	li $2,12
	li $3,122
	multu $2,$3
	nop
	li $2,61
	li $3,40
	multu $2,$3
	nop
	li $2,113
	li $3,39
	multu $2,$3
	nop
	li $2,111
	li $3,102
	multu $2,$3
	nop
	li $2,15
	li $3,58
	multu $2,$3
	nop
	li $2,63
	li $3,103
	multu $2,$3
	nop
	li $2,106
	li $3,17
	multu $2,$3
	nop
	li $2,122
	li $3,87
	multu $2,$3
	nop
	li $2,44
	li $3,18
	multu $2,$3
	nop
	li $2,25
	li $3,36
	multu $2,$3
	nop
	li $2,115
	li $3,121
	multu $2,$3
	nop
	li $2,110
	li $3,89
	multu $2,$3
	nop


	.set reorder
	.end entry
