	.text
	.align	2
	.globl	entry
	.ent	entry

	.set	noreorder
entry:


	li $3,127
	li $4,34
	sub $2,$3,$4
	
	li $3,36
	li $4,107
	sub $2,$3,$4
	
	li $3,50
	li $4,2
	sub $2,$3,$4
	
	li $3,57
	li $4,97
	sub $2,$3,$4
	
	li $3,29
	li $4,40
	sub $2,$3,$4
	
	li $3,97
	li $4,25
	sub $2,$3,$4
	
	li $3,41
	li $4,58
	sub $2,$3,$4
	
	li $3,60
	li $4,68
	sub $2,$3,$4
	
	li $2,1
	li $3,85
	divu $2,$3
	nop
	li $2,21
	li $3,89
	divu $2,$3
	nop
	li $2,68
	li $3,25
	divu $2,$3
	nop
	li $2,40
	li $3,47
	divu $2,$3
	nop
	li $2,51
	li $3,39
	multu $2,$3
	nop
	li $2,38
	li $3,37
	multu $2,$3
	nop
	li $2,83
	li $3,92
	multu $2,$3
	nop
	li $2,38
	li $3,121
	multu $2,$3
	nop
	li $2,12
	li $3,14
	or $4,$2,$3
	
	li $2,77
	li $3,71
	or $4,$2,$3
	
	li $2,100
	li $3,0
	or $4,$2,$3
	
	li $2,2
	li $3,89
	or $4,$2,$3
	


	.set reorder
	.end entry
