# set "ZAMIA_DATA_DIR=work" && del ..\ugp3.lok && zamiacad -q -f plasma_rej_userworkload_specific_overhead.py -s "design='ALU4'"
# ALU4 or ALU32 can be used instead of C17


from __future__ import with_statement
import time, os, re, sys, shutil, subprocess

yearsOfAging = 10
simtime = "10000"
testExtension = ".asm"#.tst"
overheads = [0, 0.1, 100]#[0, 0.01, 0.05, 1, 10, 100]

CurDir = "./"

# _VECTORIZED has the same initest folder
if 'sim' in globals(): design = sim

iniTestDir = CurDir + 'specific_tests/'#iniTestDir = CurDir + re.sub('_VECTORIZED', '', design) + '_tests/'
evaluations_file = iniTestDir + "individual_progress.txt"

ugpPath = CurDir + "../"
responseFile = CurDir + "to.ugp"
requestFile = ugpPath + "individualsToEvaluate.txt" #"from.ugp"
bestIndividual = ugpPath + "BEST_pop1.s"



os.path.exists(evaluations_file) and os.remove(evaluations_file)

def logEvaluation(individualID, delay):
	individualID = os.path.basename(individualID)
	msg = "%s-vec %s = %s" % (clocks, individualID, delay)
	with open(evaluations_file,'a') as f: f.write(msg+"\n")
	printf("%s-th simulation, \"%s\", is over: (%s vectors). Got  D E L A Y = %s", simNo, individualID, clocks, delay)

## simulate all vectors and collect probabilities

def ensureRemove(file):
    while (True):
        try:
            os.remove(file)
            break
        except:
            from time import ctime
            with open ("failed to remove.txt", 'a') as f: f.write ("%s: failed to remove %s\n" % (ctime(), file))
            time.sleep (.2)

FNULL = open(os.devnull,"w")

def compile(asmPath):

	with open(asmPath, 'r') as f:

		subprocess.check_call("asm.bat", stderr=FNULL, stdin=f)

	for file in os.listdir("."):
		if file == "asm.o" or file.startswith("test."):
			ensureRemove(file)

def writeResponse(response):
	print "responding " + response
	with open (responseFile, 'w') as f: f.write (response)

userLoads = [f for f in os.listdir(iniTestDir) if f.endswith(testExtension) and not "best" in f]

def stringify(zeri): return ", ".join("%s=%s" % (i, zeri[i]) for i in sorted(zeri.keys()))

rejuvenatedResults = iniTestDir + "rejevenated.txt"
dumpResults = iniTestDir + "dumpResults.txt"
with open(rejuvenatedResults,'w') as f: f.write("")

def eq( a, b, eps=0.0001 ): return abs( a - b ) <= eps # / abs(a) <= eps

def simulateRejuvenated(newTest, overhead):
	simulate(newTest)
	rejuvenated = {}
	for c in nzeri.keys():
		u_zer = userZeri[c]
		rej_zer = nzeri[c]
		user_load = 100-overhead
		rejuvenated[c] = (u_zer * user_load + rej_zer * overhead)/100

	#longestPath = recomputeLongest(rejuvenated)
	longestPath = agingAwareStatisAnalysis(rejuvenated)
	logEvaluation(os.path.splitext(ul)[0], longestPath.delay())
	#printf("%s", "%s (%s) \n mixed with %s%% of best zeri \n%s \n = \n%s \n with delay %s" % (ul, stringify(userZeri), overhead, stringify(nzeri), stringify(rejuvenated), longestPath.delay()))

	if overhead == 0:
		for i in rejuvenated: assert eq(rejuvenated[i], userZeri[i]), "rejuvenated[%s] is %s != userZeri[%s] = %s" % (i, rejuvenated[i], i, userZeri[i])
	if overhead == 100: # checks
		for i in rejuvenated: assert eq(rejuvenated[i], nzeri[i]), "rejuvenated[%s] is %s != bestZeri[%s] = %s" % (i, rejuvenated[i], i, bestZeri[i])

	return (longestPath, rejuvenated)


def newBestName(userLoad, overhead):
	return "%sbest_%s.%s%%%s" % (iniTestDir, userLoad, overhead, testExtension)

bestFilesToGenerate = [newBestName(ul, oh) for oh in overheads for ul in userLoads]
existingBestFiles = [f for f in bestFilesToGenerate if os.path.exists(f)]

if len(existingBestFiles) > 0:
	def makeBestAndRejuvenate(ul, oh):
		printf("re-simulating existing %s", newBestName(ul, oh))
		return simulateRejuvenated(newBestName(ul, oh), oh)

else:
	writeResponse("0") # dummy write to ugp before ugp is started enables requests from ugp
	#do not generate anything for 0 overhead and generate 100 overhead only once
	#n = len([1 for z in overheads if z != 0 and z != 100]) * len(userLoads) + (100 in overheads)
	n = len([1 for z in overheads if z != 0]) * len(userLoads)
	cmd = 'start cmd /K "cd %s & add_tests %s"' % (ugpPath, n)
	#from subprocess import Popen; Popen(['cmd', '/c pause'])
	import threading ; printf(cmd)
	threading.Thread(target=lambda: os.system(cmd)).start()
	printf ("Evaluator: waiting for vectors in " + requestFile + " and producing output in " + responseFile)

	def makeBestAndRejuvenate(ul, oh):
			start = time.time()

		#if oh == 0:
		#	simulate(iniTestDir + ul) ; userZeri = nzeri.copy()# produce user probabilities
		#	path = agingAwareStatisAnalysis(userZeri)
		#elif oh == 100 and overhead100 != None:
		#	path = overhead100
		#else:
			bestDelay = sys.maxint # used for checking the best individual

			while not os.path.exists(bestIndividual):

				time.sleep (.2)

				if not os.path.exists(responseFile): # uGP has consumed our evaluation. It means that it has supplied a new individual already
					with open (requestFile, 'r') as f: path = f.readline().strip()
					#executedBefore = testSize # we want to rollback executed later
					delay = simulateRejuvenated(ugpPath + path, oh)[0].delay()
					if (delay < bestDelay): bestDelay = delay
					writeResponse("%s" % fitness(delay))
					ensureRemove(requestFile) # signal that we have finished writing output
					#testSize = executedBefore # we want to rollback executed later

			newTest = newBestName(ul, oh)
			#newTest = iniTestDir + "best_" + str(len(executedAssignments)) + testExtension
			os.path.exists(newTest) and ensureRemove(newTest)
			while (True): # the fact that uGP has created the best file does not mean that it is complete. It is still writing.
				try: os.rename(bestIndividual, newTest) ; break
				except: print "failed to rename the best %s -> %s, retrying" % (bestIndividual, newTest) ; time.sleep (.2)

			path, zeri = simulateRejuvenated(newTest, oh) ; delay = path.delay()
			logEvaluation("It was the best, gen time " + str(time.time() - start), path.delay())

			#log("We have terminated %s:%s because %s exists. We grabbed it => %s after %s simulations. DELAY = %s" 			% (ul, oh, bestIndividual, newTest, simNo, delay))
			#writeSelected(str(simNo) +  "th ul was selected with " + str(simNo) + " simulations , " + fitness)

			assert eq(delay, bestDelay), "recomputed longest path delay %s mismatches the best delay %s " % (delay, bestDelay)
			return (path, zeri)

if len(existingBestFiles) > 0 and not 'sim' in globals():
	printf ("The following best exercises already exist in the output folder: %s", existingBestFiles)
	printf ("Run the script with 'sim=DesignName' instead of 'design=DesignName' to simulate them.")
else: #proceed normally

	rebuild()
	execfile(CurDir + "plasma_rej_eval.py")

	for ul in userLoads:

		ulMsg = []
		with open(dumpResults,'a') as f: f.write('\n' + ul)
		for oh in overheads:
			if oh == 0:
				simulate(iniTestDir + ul) ; userZeri = nzeri.copy()# produce user probabilities
				path = agingAwareStatisAnalysis(userZeri)
			#elif oh == 100 and overhead100 != None:
			#	path = overhead100
			else:
				(path, nzeri) = makeBestAndRejuvenate(ul, oh)

			#todo: replace zeroinc-based critical path characterization with fold-based one to make it simpler.
			def zeroinc(was, path): # was[0] contains list of path segments, w[1] is zcnt
				net = path.computedNet.net; pz = nzeri[net]
				was[0].append("(%s%s, pZ=%s)" % (path.edge, net, pz))
				return (was[0], was[1] + (1 if pz > doubleAgingProb else 0))

			zlist, zcnt = path.parent().visit( # ignore the output net, it has no Pz
				lambda path: zeroinc(([], 0), path), lambda path, prefix: zeroinc(prefix, path))
			'''
			with open(rejuvenatedResults,'a') as f:
				f.write("%s,%s nzeri=%s\n" % (ul, oh, nzeri) )
				f.write("%s,%s => %s has %s/%s zeroes:%s\n" % (ul, oh, path, zcnt, len(filter(lambda pz: pz> doubleAgingProb, nzeri.values())), zlist) )
			'''
			ulMsg.append("%s%%:(%s, %s/%s)" % (oh, path.delay(), zcnt,len(filter(lambda pz: pz> doubleAgingProb, nzeri.values()))))
			with open(dumpResults,'a') as f: f.write(", %s%%:(%s, %s/%s)" % (oh, path.delay(), zcnt,len(filter(lambda pz: pz> doubleAgingProb, nzeri.values()))))


		print "FINITOOOOOO TUTTOOOO STAMPO E ADDIO"
		#individualID = os.path.basename(individualID)
		with open(rejuvenatedResults,'a') as f: f.write(ul + ", " + ", ".join(ulMsg) + "\n")
		print "ADDIO!"
