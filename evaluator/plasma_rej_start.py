from __future__ import with_statement
import os, subprocess

tbFiles = ["tbench.vhd"]

FNULL = open(os.devnull,"w")

arg = '1'
#justSimulation = True #True

#http://superuser.com/questions/674506 explains why we need to retry
#the file delete operation

def ensureRemove(file):
    while (True):
        try:
            os.remove(file)
            break
        except:
            from time import ctime
            with open ("failed to remove.txt", 'a') as f: f.write ("%s: failed to remove %s\n" % (ctime(), file))
            time.sleep (.2)

def compile(asmPath):

	with open(asmPath, 'r') as f:

		subprocess.check_call("asm.bat", stderr=FNULL, stdin=f)

	for file in os.listdir("."):
		if file == "asm.o" or file.startswith("test."):
			ensureRemove(file)

execfile("./plasma_rej_universal_overhead.py")
