import sys,random,math


#allTypes = ['ori_add', 'li_add','ori_sub', 'li_sub', 'ori_mul','li_mul', 'ori_div','li_div', 'and', 'or', 'nor', 'xor', 'just_add', 'just_sub', 'just_mul', 'just_div']
allTypes = ['ori_add', 'li_add','ori_sub', 'li_sub', 'ori_mul','li_mul', 'ori_div','li_div', 'and', 'or', 'nor', 'xor', 'just_add', 'just_sub', 'just_mul', 'just_div']
touse = ['add', 'sub', 'div','mul', 'just_add', 'just_sub', 'just_mul', 'just_div']
intro = "\t.text\n\t.align\t2\n\t.globl\tentry\n\t.ent\tentry\n\n\t.set\tnoreorder\nentry:\n\n\n"
footer = "\n\n\t.set reorder\n\t.end entry\n"


def get_random(n_bit):
    if n_bit == 'ff':
        return '0xffffffff'
    return str(random.randint(0,math.pow(2,n_bit-1)-1))


def random_type(self):
    wtype = random.sample(allTypes, 1)[0]
    print 'set random: ' + str(wtype)
    return wtype

class WorkloadMaker:

    def new_line(self, n_bit,wl_type):
        print 'current: ' + str(wl_type)
        nop = ''
        if wl_type == 'ori_add':
            load_op1 = "ori $3,$0,"+get_random(n_bit)
            load_op2 = "ori $4,$0,"+get_random(n_bit)
            operation = "addu $2,$3,$4"
        elif wl_type == 'li_add':
            load_op1 = "li $3,"+get_random(n_bit)
            load_op2 = "li $4,"+get_random(n_bit)
            operation = "addu $2,$3,$4"

        elif wl_type == 'ori_sub':
            load_op1 = "ori $3,$0,"+get_random(n_bit)
            load_op2 = "ori $4,$0,"+get_random(n_bit)
            operation = "sub $2,$3,$4"
        elif wl_type == 'li_sub':
            load_op1 = "li $3,"+get_random(n_bit)
            load_op2 = "li $4,"+get_random(n_bit)
            operation = "sub $2,$3,$4"

        elif wl_type == 'ori_mul':
            load_op1 = "ori $2,$0,"+get_random(n_bit)
            load_op2 = "ori $3,$0,"+get_random(n_bit)
            operation = "multu $2,$3"
            nop = 'nop'
        elif wl_type == 'li_mul':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "multu $2,$3"
            nop = 'nop'

        elif wl_type == 'ori_div':
            load_op1 = "ori $2,$0,"+get_random(n_bit)
            load_op2 = "ori $3,$0,"+get_random(n_bit)
            operation = "divu $2,$3"
            nop = 'nop'
        elif wl_type == 'li_div':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "divu $2,$3"
            nop = 'nop'

        elif wl_type == 'and':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "and $4,$2,$3"
        elif wl_type == 'or':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "or $4,$2,$3"
        elif wl_type == 'xor':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "xor $4,$2,$3"
        elif wl_type == 'nor':
            load_op1 = "li $2,"+get_random(n_bit)
            load_op2 = "li $3,"+get_random(n_bit)
            operation = "nor $4,$2,$3"
        elif wl_type == 'just_add':
            load_op1 = ''
            load_op2 = ''
            operation = "addu $2,$3,$4"
        elif wl_type == 'just_sub':
            load_op1 = ''
            load_op2 = ''
            operation = "sub $2,$3,$4"
        elif wl_type == 'just_mul':
            load_op1 = ''
            load_op2 = ''
            operation = "multu $2,$3"
            nop = 'nop'
        elif wl_type == 'just_div':
            load_op1 = ''
            load_op2 = ''
            operation = "divu $2,$3"
            nop = 'nop'
        elif wl_type == 'init_2_3':
            return '\tli $2,'+get_random(n_bit)+'\n\tli $3,'+get_random(n_bit)+'\n'
        elif wl_type == 'start_count':
            return '\tli $2,0\n\taddi $2,$2,1\n'
        elif wl_type == 'count':
            return '\taddi $2,$2,1\n'
        else:
            print 'errore!'
            return
        line = '\t' + load_op1 + '\n\t' + load_op2 + '\n\t' + operation + '\n\t' + nop + '\n'
        return line

def workload_producer(filename, mode, init23, n_bit, n_op, header_footer):

    wlm = WorkloadMaker()

    with open(filename, 'w') as f:
        if header_footer:
            f.write (intro)
        body = ""
        for wl_t in mode:
            if wl_t == 'count':
                body += wlm.new_line(n_bit,'start_count')
            elif init23:
                body += wlm.new_line(n_bit,'init_2_3')
            for i in range(0,n_op):
                body += wlm.new_line(n_bit,wl_t)
        f.write(body)
        if header_footer: f.write(footer)


testExtension = '.asm'
#wl_dir = 'workloads/full_FF_operands/'
wl_dir = 'workloads/no_header_no_footer/'

#traslate = {10:'short', 100:'long'}
no = 20


#workload_producer(wl_dir + str('count') + testExtension, {'count'},8,
#
# for type in ['just_mul', 'just_sub']:
#     for nbit in [1, 4, 32]:
#         workload_producer(wl_dir + type + '_' + str(nbit) + testExtension, {type}, nbit, no)
#


# workload_producer(wl_dir + 'products' + testExtension, {'just_mul'}, True, 8, no)
# workload_producer(wl_dir + 'subseq' + testExtension, {'just_sub'}, True, 8, no)
# workload_producer(wl_dir + 'count' + testExtension, {'count'}, True, 8, no)

# for t in allTypes:
#     if 'just' in t:
#         workload_producer(wl_dir + t + '_ff' + testExtension, {t}, True, 'ff', no, False)
#     else:
#         workload_producer(wl_dir + t + '_ff' + testExtension, {t}, False, 'ff', no, False)

workload_producer("./wl_tests/mixed1.asm",{'li_add','li_sub','li_mul','li_div'},False, 16, 1, False)