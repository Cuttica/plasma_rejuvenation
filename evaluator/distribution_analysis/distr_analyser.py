from __future__ import division
import os

DirsToAnalyse = ["cat_1/", "cat_2/", "cat_3/"]
CurDir = "./"

countingTable = {}

def analyseAssebler(filename, tot):
    print "open: " + filename
    with open(filename, 'r') as f:
        text = f.read()
        for line in text.split("\n"):
            if ':' not in line and line.strip() != "":
                res = line.strip().split(" ")
                instruction = res[0].strip()
                if instruction in countingTable.keys():
                    countingTable[instruction] += 1
                    print "incremento " + instruction
                else:
                    countingTable[instruction] = 1
                    print "nuovo ele: " + instruction
                tot += 1
        return tot


def printResults(filename,tot):
    with open(filename, "w") as f:
        txt = "instruction, freq, , instruction, percentage,\n"

        for instr in countingTable.keys():
            counting = countingTable[instr]
            print str(counting) + " - " + str(tot) + " - " + str(float(counting / tot))
            txt += instr + ", " + str(counting) + ", ," + instr + ", " + str(counting / tot * 100) + ", \n"
        f.write(txt)


for dir in DirsToAnalyse:
    countingTable.clear()
    tot = 0
    for file in os.listdir(CurDir + dir):
        if file.endswith(".asm"):
            tot = analyseAssebler(CurDir + dir + file, tot)
    printResults(CurDir + dir + "results.csv", tot)
