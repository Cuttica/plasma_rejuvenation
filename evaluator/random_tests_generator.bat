
: Evaluator simulates a prescribed set of tests at start and produces to.ugp file 
: with their Hamming. You may ignore it. When that file disappears, it is expected 
: that uGP has prepared the next test for evaluation in 'from.gp'. This file is simulated 
: and Hamming is produced. Waiting for 'to.gp' removal and writing to 'from.gp' 
: continues until there is not 'from.gp' after 'to.gp' removal.

setlocal
set path=%~dp0/asm;c:\cygwin64\bin;%path%

:classpath to current dir is needed for classes in distutils
set CLASSPATH=%~dp0

::generate random number, suitable for parallel execution, http://stackoverflow.com/a/19705062/1083704
@for /f %%i in ('C:\cygwin64\bin\bash -c "echo $RANDOM"') do @set CYGWIN_RANDOM=%%i
:set ZAMIA_DATA_DIR=zamia_temp-%CYGWIN_RANDOM%

: my working zamia
set args=-p "PlasmaEvaluator-%CYGWIN_RANDOM%" -q -f plasma_rej_start.py -s "justSimulation=False" "random_tests=True" "worst_fitness=False"

:if "%1"=="-s" set args=-p "PlasmaEvaluator-%CYGWIN_RANDOM%" -q -f plasma_rej_start.py -s "justSimulation=True"
:if "%2"=="-s" set args=-p "PlasmaEvaluator-%CYGWIN_RANDOM%" -q -f plasma_rej_start.py -s "justSimulation=True"

:call C:\Users\valentin\workspace\zamiacad\zamiacad.bat %args%
:call ..\..\zamiacad\zamiacad.bat %args%
call zamiacad.bat %args%

: They can also use the GUI

:if you want to use Plasma + dump for ALU-Rejuvenation uncomment these following lines

:cd D:\Documenti\GitHub\ugp-rejuvenation\evaluator
:d:
:call D:\Documenti\GitHub\ugp-rejuvenation\evaluator\gen_universal.bat ALU32 -best 1


:if "%1"=="-g" call python paths_visualizer.py
:if "%2"=="-g" call python paths_visualizer.py

endlocal