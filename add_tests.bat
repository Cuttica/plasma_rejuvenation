: Parameters
:	- the simulation length of every individual program in evaluator (nanoseconds)
:	- the genration size (mu, nu) and number of generations in uGP
:	- initial tests
:	- the amount of tests to add (supergenerations, chosen here)


@echo off
setlocal
set path=c:\cygwin64\bin;%path%

IF "%*"=="" (
	set SELF=%~n0
	echo The program calls uGP n times and exits. Every time uGP is called, an asm is generated for plasma many times and one with the best hamming score is selected in the end
	echo Usage: %SELF% num
	echo For instance, to add 3 tests: %SELF% 3
	goto exit
)
echo %~dp0
set MY_TMP_PATH=%~dp0
echo "%MY_TMP_PATH%"
set classpath=%~dp0;%classpath%
for /L %%I in (1,1,%1) do (
	rm -f individuals/*.s
	"%MY_TMP_PATH%"..\ugp3
)
endlocal
echo done done

:exit